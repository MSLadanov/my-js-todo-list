// # Globals

const todoList = document.getElementById('todo-list');
const userSelect = document.getElementById('user-todo');
const form = document.querySelector('form');
const input = document.querySelector("#new-todo");
const button = document.querySelector(".addtodo");
let todos = [];
let users = [];

// Event Listeners

document.addEventListener('DOMContentLoaded', init);
form.addEventListener('submit', handleSubmit);
userSelect.addEventListener('change', sortedTodos)

// Basic Logic

function getUserName(userId) {
  const user = users.find((u) => u.id === userId);
  return user.name;
}
function todoItemRender({ id, userId, title, completed }) {
  const li = document.createElement('li');
  li.className = 'todo-item';
  li.dataset.id = id;
  li.innerHTML = `<span>${title} <b><u> for</u></b> : <i>${getUserName(
    userId
  )}</i></span>`;

  const status = document.createElement('input');
  status.type = 'checkbox';
  status.checked = completed;
  status.addEventListener('change', handleTodoChange);

  const close = document.createElement('span');
  close.innerHTML = '&#10008;';
  close.className = 'close';
  close.addEventListener('click', handleClose);

  li.prepend(status);
  li.append(close);

  todoList.prepend(li);
}

function userList(user) {
  const option = document.createElement('option');
  option.value = user.id;
  option.innerText = user.name;
  
  userSelect.append(option);
}

function clearElement(element) {
  element.innerHTML = "";
}

function sortedTodos() {
  const choosedUserId = Number(this.value)
  const userTodos = todos.filter((todo) => todo.userId === choosedUserId)
  clearElement(todoList)
  userTodos.forEach((todo) => todoItemRender(todo));
  input.style.display = "block";
  button.style.display = "block";
}

function removeTodo(todoId) {
  todos = todos.filter((todo) => todo.id !== todoId);
  const todo = todoList.querySelector(`[data-id="${todoId}"]`);
  todo.querySelector('input').removeEventListener('change', handleTodoChange);
  todo.querySelector('.close').removeEventListener('click', handleClose);

  todo.remove();
}

function alertError(error) {
  alert(error.message);
}

function init() {
  Promise.all([getAllTodos(), getAllUsers()]).then((values) => {
    [todos, users] = values;
    input.style.display = "none";
    button.style.display = "none";
    users.forEach((user) => userList(user));
  });
}

// # handlers

function handleSubmit(event) {
  event.preventDefault();
  createTodo({
    userId: Number(form.user.value),
    title: form.todo.value,
    completed: false,
  });
}
function handleTodoChange() {
  const todoId = this.parentElement.dataset.id;
  const completed = this.checked;

  toggleTodoComplete(todoId, completed);
}
function handleClose() {
  const todoId = this.parentElement.dataset.id;
  deleteTodo(todoId);
}

//# async logic

async function getAllTodos() {
  try {
    const response = await fetch(
      'https://jsonplaceholder.typicode.com/todos'
    );
    const data = await response.json();

    return data;
  } catch (error) {
    alertError(error);
  }
}

async function getAllUsers() {
  try {
    const response = await fetch(
      'https://jsonplaceholder.typicode.com/users'
    );
    const data = await response.json();

    return data;
  } catch (error) {
    alertError(error);
  }
}

async function createTodo(todo) {
  try {
    const response = await fetch(
      'https://jsonplaceholder.typicode.com/todos',
      {
        method: 'POST',
        body: JSON.stringify(todo),
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );

    const newTodo = await response.json();

    todoItemRender(newTodo);
  } catch (error) {
    alertError(error);
  }
}

async function toggleTodoComplete(todoId, completed) {
  try {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/todos/${todoId}`,
      {
        method: 'PATCH',
        body: JSON.stringify({ completed }),
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );

    if (!response.ok) {
      throw new Error('Failed to connect with the server! Please try later.');
    }
  } catch (error) {
    alertError(error);
  }
}

async function deleteTodo(todoId) {
  try {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/todos/${todoId}`,
      {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
      }
    );

    if (response.ok) {
      removeTodo(todoId);
    } else {
      throw new Error('Failed to connect with the server! Please try later.');
    }
  } catch (error) {
    alertError(error);
  }
}
